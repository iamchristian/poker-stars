import { useOddsData } from "../hooks/useOdds";

function TopGames(): React.ReactElement {
  const { ids, data } = useOddsData([5, 6, 9]);

  return (
    <>
      <h2 className="mb-4 text-2xl">Top Games</h2>
      <ul className="mb-4 divide-y-4 divide-white">
        {Array.from(ids).map((id) => (
          <li key={`top-games-${id}`} className="flex">
            <span className="w-10 p-1 text-center bg-gray-200 rounded-l">
              {id}
            </span>
            <span className="flex-grow p-1 bg-gray-50">
              {data[id] ?? "Loading"}
            </span>
          </li>
        ))}
      </ul>
    </>
  );
}

export default TopGames;
