import {
  createContext,
  Dispatch,
  MutableRefObject,
  SetStateAction,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { Game } from "../pages/api/data";

//TODO: Add an abort-controller to cancel on unmount.
async function getData(ids: number[]): Promise<Game[]> {
  return (await fetch(`/api/data?ids=${ids.join(",")}`)).json();
}

const OddsContext = createContext<{
  updateIds: (
    componentRef: MutableRefObject<undefined>,
    componentIds: Set<number>
  ) => void;
  data: { [key: string]: string };
  untrack: (componentRef: MutableRefObject<undefined>) => void;
}>({
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  updateIds: () => {},
  data: {},
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  untrack: () => {},
});

export const OddsProvider: React.FC = function OddsProvider({ children }) {
  const [ids, setIds] = useState<Map<MutableRefObject<undefined>, Set<number>>>(
    new Map()
  );
  const [oddsData, setOddsData] = useState<Record<string, string>>({});

  const timeoutRef = useRef<number>();

  const handleIdUpdates = useCallback(
    (componentRef: MutableRefObject<undefined>, componentIds: Set<number>) => {
      setIds((idMap) => new Map(idMap).set(componentRef, componentIds));
    },
    [setIds]
  );

  const updateOddsData = useCallback(() => {
    window.clearTimeout(timeoutRef.current);

    const uniqueIds: Set<number> = new Set(
      Array.from(ids.values())
        .map((i) => Array.from(i))
        .flat()
    );

    if (uniqueIds.size > 0) {
      console.log("Getting data");
      getData(Array.from(uniqueIds)).then((data) => {
        const zippedData = data.reduce<{ [key: string]: string }>(
          (acc, val) => ({
            ...acc,
            [val.id]: val.value,
          }),
          {}
        );

        setOddsData(zippedData);

        timeoutRef.current = window.setTimeout(() => {
          updateOddsData();
        }, 1000 * 5);
      });
    }
  }, [ids]);

  const untrack = useCallback(
    (componentRef: MutableRefObject<undefined>) => {
      setIds((idMap) => {
        const idMapCopy = new Map(idMap);
        idMapCopy.delete(componentRef);
        return idMapCopy;
      });
    },
    [setIds]
  );

  useEffect(() => {
    updateOddsData();
  }, [ids, updateOddsData]);

  return (
    <OddsContext.Provider
      value={{ updateIds: handleIdUpdates, data: oddsData, untrack }}
    >
      {children}
    </OddsContext.Provider>
  );
};

export function useOddsData(
  initialIds: number[] = []
): {
  ids: Set<number>;
  data: {
    [key: string]: string;
  };
  setIds: Dispatch<SetStateAction<Set<number>>>;
} {
  const { data, updateIds, untrack } = useContext(OddsContext);
  const [ids, setIds] = useState<Set<number>>(new Set(initialIds));
  const uniqueId = useRef();

  useEffect(() => {
    updateIds(uniqueId, ids);
  }, [ids, updateIds]);

  useEffect(() => {
    return () => {
      untrack(uniqueId);
    };
  }, [untrack]);

  return {
    ids,
    data,
    setIds,
  };
}
