module.exports = {
  "*.+(ts|tsx|js|jsx)": ["eslint --fix", "jest --findRelatedTests"],
};
