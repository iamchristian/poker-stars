/* eslint-disable @typescript-eslint/no-empty-function */
import "@testing-library/jest-dom";
import { render, screen, within } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import GameCard from "../GameCard";

describe("GameCard", () => {
  it("should add an item to the list on button click", () => {
    render(<GameCard id="gamecard-1" />);

    userEvent.type(screen.getByRole("textbox", { name: "Data ID" }), "1");
    userEvent.click(screen.getByRole("button", { name: "Add ID" }));

    const list = screen.getByRole("list", { name: "Subscriptions" });

    const { getAllByRole: getAllByRoleWithinList } = within(list);

    let listItems = getAllByRoleWithinList("listitem");

    expect(listItems).toHaveLength(1);
    expect(listItems[0]).toHaveTextContent("1");

    userEvent.type(screen.getByRole("textbox", { name: "Data ID" }), "3");
    userEvent.click(screen.getByRole("button", { name: "Add ID" }));

    listItems = getAllByRoleWithinList("listitem");

    expect(listItems).toHaveLength(2);
    expect(listItems[1]).toHaveTextContent("3");
  });

  it("should not add anything to the list if the textbox is empty", () => {
    render(<GameCard id="gamecard-1" />);

    userEvent.click(screen.getByRole("button", { name: "Add ID" }));

    const list = screen.getByRole("list", { name: "Subscriptions" });

    const { queryAllByRole: queryAllByRoleWithinList } = within(list);

    const listItems = queryAllByRoleWithinList("listitem");

    expect(listItems).toHaveLength(0);
  });

  it("should not add anything to the list if the textbox contains text", () => {
    render(<GameCard id="gamecard-1" />);

    userEvent.type(screen.getByRole("textbox", { name: "Data ID" }), "Dave");
    userEvent.click(screen.getByRole("button", { name: "Add ID" }));

    const list = screen.getByRole("list", { name: "Subscriptions" });

    const { queryAllByRole: queryAllByRoleWithinList } = within(list);

    const listItems = queryAllByRoleWithinList("listitem");

    expect(listItems).toHaveLength(0);
  });

  it("should reset the value of the textbox after the item is added", () => {
    render(<GameCard id="gamecard-1" />);

    userEvent.type(screen.getByRole("textbox", { name: "Data ID" }), "1");
    userEvent.click(screen.getByRole("button", { name: "Add ID" }));

    expect(screen.getByRole("textbox", { name: "Data ID" })).toHaveValue("");
  });

  it("should emove an item to the list when the delete button is clicked", () => {
    render(<GameCard id="gamecard-1" />);

    userEvent.type(screen.getByRole("textbox", { name: "Data ID" }), "1");
    userEvent.click(screen.getByRole("button", { name: "Add ID" }));

    const list = screen.getByRole("list", { name: "Subscriptions" });

    const {
      getAllByRole: getAllByRoleWithinList,
      queryAllByRole: queryAllByRoleWithinList,
    } = within(list);

    let listItems = getAllByRoleWithinList("listitem");

    expect(listItems).toHaveLength(1);

    userEvent.click(screen.getByRole("button", { name: "Remove ID 1" }));

    listItems = queryAllByRoleWithinList("listitem");

    expect(listItems).toHaveLength(0);
  });
});
