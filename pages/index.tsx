import Head from "next/head";
import { useState } from "react";
import GameCard from "../components/GameCard";
import TopGames from "../components/TopGames";
import { OddsProvider } from "../hooks/useOdds";

export default function Home(): React.ReactElement {
  const [gameCardCount, setGameCardCount] = useState(1);
  return (
    <>
      <Head>
        <title>Odds Tracker</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="container mx-auto mt-4">
        <OddsProvider>
          <TopGames />
          <div className="space-x-2">
            <button
              type="button"
              onClick={() => {
                setGameCardCount((c) => c + 1);
              }}
              className="px-4 py-3 bg-green-300 rounded"
            >
              Add Game Card
            </button>
            <button
              type="button"
              onClick={() => {
                setGameCardCount((c) => Math.max(c - 1, 0));
              }}
              className="px-4 py-3 bg-red-300 rounded"
            >
              Remove Game Card
            </button>
          </div>
          {Array.from({ length: gameCardCount }, (_, i) => (
            <GameCard key={`gamecard-${i}`} id={`gamecard-${i}`} />
          ))}
        </OddsProvider>
      </main>
    </>
  );
}
