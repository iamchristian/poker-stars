import type { NextApiRequest, NextApiResponse } from "next";
import * as faker from "faker";

export interface Game {
  id: number;
  value: number;
}

export default (req: NextApiRequest, res: NextApiResponse<Game[]>) => {
  const ids: string = req.query.ids as string;

  if (!ids) {
    return res.status(200).json([]);
  }

  return res
    .status(200)
    .json(
      ids
        .split(",")
        .reduce<Game[]>(
          (acc, id) => [
            ...acc,
            { id: Number(id), value: faker.random.number() },
          ],
          []
        )
    );
};
