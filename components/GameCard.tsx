import { useRef } from "react";
import { useOddsData } from "../hooks/useOdds";

export function GameCard({ id }: { id: string }): React.ReactElement {
  const { data, setIds, ids } = useOddsData();

  const idInput = useRef<HTMLInputElement>(null);

  function handleAddIdClick() {
    const value = idInput.current.value;

    if (!value || Number.isNaN(Number(value))) return;

    setIds((i) => new Set(i).add(Number(value)));

    idInput.current.value = "";
  }

  function handleRemoveIdClick(id: number) {
    setIds((i) => {
      const newIds = new Set(i);
      newIds.delete(id);
      return newIds;
    });
  }

  return (
    <>
      <h2 className="mb-4 text-2xl">Data Subscriber</h2>
      <div className="flex mb-4">
        <label
          className="p-1.5 px-5 bg-gray-200 rounded-l"
          htmlFor={`hash-id-input-${id}`}
        >
          Data ID
        </label>
        <input
          className="flex-grow p-1 mr-3 border-2 border-solid rounded-r"
          placeholder="1"
          ref={idInput}
          id={`hash-id-input-${id}`}
        />
        <button
          type="button"
          className="bg-green-300 rounded w-14"
          aria-label="Add ID"
          onClick={() => handleAddIdClick()}
        >
          +
        </button>
      </div>
      <div>
        <h3 className="mb-1 text-l">Subscriptions</h3>
        <ul className="mb-4 divide-y-4 divide-white" aria-label="Subscriptions">
          {Array.from(ids).map((id) => (
            <li key={id} className="flex">
              <span className="w-10 p-1 text-center bg-gray-200 rounded-l">
                {id}
              </span>
              <span className="flex-grow p-1 bg-gray-50">
                {data[id] ?? "Loading"}
              </span>
              <button
                className="bg-red-300 rounded-r w-14"
                aria-label={`Remove ID ${id}`}
                onClick={() => {
                  handleRemoveIdClick(id);
                }}
              >
                -
              </button>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default GameCard;
